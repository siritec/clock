#![allow(bad_style)]

pub use self::inner::*;

#[cfg(target_os = "redox")]
mod inner {
    use std::fmt;
    use std::cmp::Ordering;
    use std::ops::{Add, Sub};
    use syscall;

    use Duration;
    use Tm;

    fn time_to_tm(ts: i64, tm: &mut Tm) {
        let leapyear = |year| -> bool {
            year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)
        };

        static _ytab: [[i64; 12]; 2] = [
            [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ],
            [ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ]
        ];

        let mut year = 1970;

        let dayclock = ts % 86400;
        let mut dayno = ts / 86400;

        tm.tm_sec = (dayclock % 60) as i32;
        tm.tm_min = ((dayclock % 3600) / 60) as i32;
        tm.tm_hour = (dayclock / 3600) as i32;
        tm.tm_wday = ((dayno + 4) % 7) as i32;
        loop {
            let yearsize = if leapyear(year) {
                366
            } else {
                365
            };
            if dayno >= yearsize {
                    dayno -= yearsize;
                    year += 1;
            } else {
                break;
            }
        }
        tm.tm_year = (year - 1900) as i32;
        tm.tm_yday = dayno as i32;
        let mut mon = 0;
        while dayno >= _ytab[if leapyear(year) { 1 } else { 0 }][mon] {
                dayno -= _ytab[if leapyear(year) { 1 } else { 0 }][mon];
                mon += 1;
        }
        tm.tm_mon = mon as i32;
        tm.tm_mday = dayno as i32 + 1;
        tm.tm_isdst = 0;
    }

    fn tm_to_time(tm: &Tm) -> i64 {
        let mut y = tm.tm_year as i64 + 1900;
        let mut m = tm.tm_mon as i64 + 1;
        if m <= 2 {
            y -= 1;
            m += 12;
        }
        let d = tm.tm_mday as i64;
        let h = tm.tm_hour as i64;
        let mi = tm.tm_min as i64;
        let s = tm.tm_sec as i64;
        (365*y + y/4 - y/100 + y/400 + 3*(m+1)/5 + 30*m + d - 719561)
            * 86400 + 3600 * h + 60 * mi + s
    }

    pub fn time_to_utc_tm(sec: i64, tm: &mut Tm) {
        time_to_tm(sec, tm);
    }

    pub fn time_to_local_tm(sec: i64, tm: &mut Tm) {
        // FIXME: Add timezone logic
        time_to_tm(sec, tm);
    }

    pub fn utc_tm_to_time(tm: &Tm) -> i64 {
        tm_to_time(tm)
    }

    pub fn local_tm_to_time(tm: &Tm) -> i64 {
        // FIXME: Add timezone logic
        tm_to_time(tm)
    }

    pub fn get_time() -> (i64, i32) {
        let mut tv = syscall::TimeSpec { tv_sec: 0, tv_nsec: 0 };
        syscall::clock_gettime(syscall::CLOCK_REALTIME, &mut tv).unwrap();
        (tv.tv_sec as i64, tv.tv_nsec as i32)
    }

    pub fn get_precise_ns() -> u64 {
        let mut ts = syscall::TimeSpec { tv_sec: 0, tv_nsec: 0 };
        syscall::clock_gettime(syscall::CLOCK_MONOTONIC, &mut ts).unwrap();
        (ts.tv_sec as u64) * 1000000000 + (ts.tv_nsec as u64)
    }

    #[derive(Copy)]
    pub struct SteadyTime {
        t: syscall::TimeSpec,
    }

    impl fmt::Debug for SteadyTime {
        fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
            write!(fmt, "SteadyTime {{ tv_sec: {:?}, tv_nsec: {:?} }}",
                   self.t.tv_sec, self.t.tv_nsec)
        }
    }

    impl Clone for SteadyTime {
        fn clone(&self) -> SteadyTime {
            SteadyTime { t: self.t }
        }
    }

    impl SteadyTime {
        pub fn now() -> SteadyTime {
            let mut t = SteadyTime {
                t: syscall::TimeSpec {
                    tv_sec: 0,
                    tv_nsec: 0,
                }
            };
            syscall::clock_gettime(syscall::CLOCK_MONOTONIC, &mut t.t).unwrap();
            t
        }
    }

    impl Sub for SteadyTime {
        type Output = Duration;
        fn sub(self, other: SteadyTime) -> Duration {
            if self.t.tv_nsec >= other.t.tv_nsec {
                Duration::seconds(self.t.tv_sec as i64 - other.t.tv_sec as i64) +
                    Duration::nanoseconds(self.t.tv_nsec as i64 - other.t.tv_nsec as i64)
            } else {
                Duration::seconds(self.t.tv_sec as i64 - 1 - other.t.tv_sec as i64) +
                    Duration::nanoseconds(self.t.tv_nsec as i64 + ::NSEC_PER_SEC as i64 -
                                          other.t.tv_nsec as i64)
            }
        }
    }

    impl Sub<Duration> for SteadyTime {
        type Output = SteadyTime;
        fn sub(self, other: Duration) -> SteadyTime {
            self + -other
        }
    }

    impl Add<Duration> for SteadyTime {
        type Output = SteadyTime;
        fn add(mut self, other: Duration) -> SteadyTime {
            let seconds = other.num_seconds();
            let nanoseconds = other - Duration::seconds(seconds);
            let nanoseconds = nanoseconds.num_nanoseconds().unwrap();
            self.t.tv_sec += seconds;
            self.t.tv_nsec += nanoseconds as i32;
            if self.t.tv_nsec >= ::NSEC_PER_SEC {
                self.t.tv_nsec -= ::NSEC_PER_SEC;
                self.t.tv_sec += 1;
            } else if self.t.tv_nsec < 0 {
                self.t.tv_sec -= 1;
                self.t.tv_nsec += ::NSEC_PER_SEC;
            }
            self
        }
    }

    impl PartialOrd for SteadyTime {
        fn partial_cmp(&self, other: &SteadyTime) -> Option<Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Ord for SteadyTime {
        fn cmp(&self, other: &SteadyTime) -> Ordering {
            match self.t.tv_sec.cmp(&other.t.tv_sec) {
                Ordering::Equal => self.t.tv_nsec.cmp(&other.t.tv_nsec),
                ord => ord
            }
        }
    }

    impl PartialEq for SteadyTime {
        fn eq(&self, other: &SteadyTime) -> bool {
            self.t.tv_sec == other.t.tv_sec &&
                self.t.tv_nsec == other.t.tv_nsec
        }
    }

    impl Eq for SteadyTime {}
}

#[cfg(unix)]
mod inner {
    use libc::{self, time_t};
    use std::mem;
    use std::io;
    use Tm;

    #[cfg(any(target_os = "macos", target_os = "ios"))]
    pub use self::mac::*;
    #[cfg(all(not(target_os = "macos"), not(target_os = "ios")))]
    pub use self::unix::*;

    #[cfg(target_os = "solaris")]
    extern {
        static timezone: time_t;
        static altzone: time_t;
    }

    fn rust_tm_to_tm(rust_tm: &Tm, tm: &mut libc::tm) {
        tm.tm_sec = rust_tm.tm_sec;
        tm.tm_min = rust_tm.tm_min;
        tm.tm_hour = rust_tm.tm_hour;
        tm.tm_mday = rust_tm.tm_mday;
        tm.tm_mon = rust_tm.tm_mon;
        tm.tm_year = rust_tm.tm_year;
        tm.tm_wday = rust_tm.tm_wday;
        tm.tm_yday = rust_tm.tm_yday;
        tm.tm_isdst = rust_tm.tm_isdst;
    }

    fn tm_to_rust_tm(tm: &libc::tm, utcoff: i32, rust_tm: &mut Tm) {
        rust_tm.tm_sec = tm.tm_sec;
        rust_tm.tm_min = tm.tm_min;
        rust_tm.tm_hour = tm.tm_hour;
        rust_tm.tm_mday = tm.tm_mday;
        rust_tm.tm_mon = tm.tm_mon;
        rust_tm.tm_year = tm.tm_year;
        rust_tm.tm_wday = tm.tm_wday;
        rust_tm.tm_yday = tm.tm_yday;
        rust_tm.tm_isdst = tm.tm_isdst;
        rust_tm.tm_utcoff = utcoff;
    }

    #[cfg(any(target_os = "nacl", target_os = "solaris"))]
    unsafe fn timegm(tm: *mut libc::tm) -> time_t {
        use std::env::{set_var, var_os, remove_var};
        extern {
            fn tzset();
        }

        let ret;

        let current_tz = var_os("TZ");
        set_var("TZ", "UTC");
        tzset();

        ret = libc::mktime(tm);

        if let Some(tz) = current_tz {
            set_var("TZ", tz);
        } else {
            remove_var("TZ");
        }
        tzset();

        ret
    }

    pub fn time_to_utc_tm(sec: i64, tm: &mut Tm) {
        unsafe {
            let sec = sec as time_t;
            let mut out = mem::zeroed();
            if libc::gmtime_r(&sec, &mut out).is_null() {
                panic!("gmtime_r failed: {}", io::Error::last_os_error());
            }
            tm_to_rust_tm(&out, 0, tm);
        }
    }

    pub fn time_to_local_tm(sec: i64, tm: &mut Tm) {
        unsafe {
            let sec = sec as time_t;
            let mut out = mem::zeroed();
            if libc::localtime_r(&sec, &mut out).is_null() {
                panic!("localtime_r failed: {}", io::Error::last_os_error());
            }
            #[cfg(target_os = "solaris")]
            let gmtoff = {
                ::tzset();
                // < 0 means we don't know; assume we're not in DST.
                if out.tm_isdst == 0 {
                    // timezone is seconds west of UTC, tm_gmtoff is seconds east
                    -timezone
                } else if out.tm_isdst > 0 {
                    -altzone
                } else {
                    -timezone
                }
            };
            #[cfg(not(target_os = "solaris"))]
            let gmtoff = out.tm_gmtoff;
            tm_to_rust_tm(&out, gmtoff as i32, tm);
        }
    }

    pub fn utc_tm_to_time(rust_tm: &Tm) -> i64 {
        #[cfg(all(target_os = "android", target_pointer_width = "32"))]
        use libc::timegm64 as timegm;
        #[cfg(not(any(all(target_os = "android", target_pointer_width = "32"), target_os = "nacl", target_os = "solaris")))]
        use libc::timegm;

        let mut tm = unsafe { mem::zeroed() };
        rust_tm_to_tm(rust_tm, &mut tm);
        unsafe { timegm(&mut tm) as i64 }
    }

    pub fn local_tm_to_time(rust_tm: &Tm) -> i64 {
        let mut tm = unsafe { mem::zeroed() };
        rust_tm_to_tm(rust_tm, &mut tm);
        unsafe { libc::mktime(&mut tm) as i64 }
    }

    #[cfg(any(target_os = "macos", target_os = "ios"))]
    mod mac {
        use libc::{self, timeval, mach_timebase_info};
        use std::sync::{Once, ONCE_INIT};
        use std::ops::{Add, Sub};
        use Duration;

        fn info() -> &'static mach_timebase_info {
            static mut INFO: mach_timebase_info = mach_timebase_info {
                numer: 0,
                denom: 0,
            };
            static ONCE: Once = ONCE_INIT;

            unsafe {
                ONCE.call_once(|| {
                    mach_timebase_info(&mut INFO);
                });
                &INFO
            }
        }

        pub fn get_time() -> (i64, i32) {
            use std::ptr;
            let mut tv = timeval { tv_sec: 0, tv_usec: 0 };
            unsafe { libc::gettimeofday(&mut tv, ptr::null_mut()); }
            (tv.tv_sec as i64, tv.tv_usec * 1000)
        }

        #[inline]
        pub fn get_precise_ns() -> u64 {
            unsafe {
                let time = libc::mach_absolute_time();
                let info = info();
                time * info.numer as u64 / info.denom as u64
            }
        }

        #[derive(Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Debug)]
        pub struct SteadyTime { t: u64 }

        impl SteadyTime {
            pub fn now() -> SteadyTime {
                SteadyTime { t: get_precise_ns() }
            }
        }
        impl Sub for SteadyTime {
            type Output = Duration;
            fn sub(self, other: SteadyTime) -> Duration {
                Duration::nanoseconds(self.t as i64 - other.t as i64)
            }
        }
        impl Sub<Duration> for SteadyTime {
            type Output = SteadyTime;
            fn sub(self, other: Duration) -> SteadyTime {
                self + -other
            }
        }
        impl Add<Duration> for SteadyTime {
            type Output = SteadyTime;
            fn add(self, other: Duration) -> SteadyTime {
                let delta = other.num_nanoseconds().unwrap();
                SteadyTime {
                    t: (self.t as i64 + delta) as u64
                }
            }
        }
    }

    #[cfg(test)]
    pub struct TzReset;

    #[cfg(test)]
    pub fn set_los_angeles_time_zone() -> TzReset {
        use std::env;
        env::set_var("TZ", "America/Los_Angeles");
        ::tzset();
        TzReset
    }

    #[cfg(test)]
    pub fn set_london_with_dst_time_zone() -> TzReset {
        use std::env;
        env::set_var("TZ", "Europe/London");
        ::tzset();
        TzReset
    }

    #[cfg(all(not(target_os = "macos"), not(target_os = "ios")))]
    mod unix {
        use std::fmt;
        use std::cmp::Ordering;
        use std::ops::{Add, Sub};
        use libc;

        use Duration;

        pub fn get_time() -> (i64, i32) {
            let mut tv = libc::timespec { tv_sec: 0, tv_nsec: 0 };
            unsafe { libc::clock_gettime(libc::CLOCK_REALTIME, &mut tv); }
            (tv.tv_sec as i64, tv.tv_nsec as i32)
        }

        pub fn get_precise_ns() -> u64 {
            let mut ts = libc::timespec { tv_sec: 0, tv_nsec: 0 };
            unsafe {
                libc::clock_gettime(libc::CLOCK_MONOTONIC, &mut ts);
            }
            (ts.tv_sec as u64) * 1000000000 + (ts.tv_nsec as u64)
        }

        #[derive(Copy)]
        pub struct SteadyTime {
            t: libc::timespec,
        }

        impl fmt::Debug for SteadyTime {
            fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
                write!(fmt, "SteadyTime {{ tv_sec: {:?}, tv_nsec: {:?} }}",
                       self.t.tv_sec, self.t.tv_nsec)
            }
        }

        impl Clone for SteadyTime {
            fn clone(&self) -> SteadyTime {
                SteadyTime { t: self.t }
            }
        }

        impl SteadyTime {
            pub fn now() -> SteadyTime {
                let mut t = SteadyTime {
                    t: libc::timespec {
                        tv_sec: 0,
                        tv_nsec: 0,
                    }
                };
                unsafe {
                    assert_eq!(0, libc::clock_gettime(libc::CLOCK_MONOTONIC,
                                                      &mut t.t));
                }
                t
            }
        }

        impl Sub for SteadyTime {
            type Output = Duration;
            fn sub(self, other: SteadyTime) -> Duration {
                if self.t.tv_nsec >= other.t.tv_nsec {
                    Duration::seconds(self.t.tv_sec as i64 - other.t.tv_sec as i64) +
                        Duration::nanoseconds(self.t.tv_nsec as i64 - other.t.tv_nsec as i64)
                } else {
                    Duration::seconds(self.t.tv_sec as i64 - 1 - other.t.tv_sec as i64) +
                        Duration::nanoseconds(self.t.tv_nsec as i64 + ::NSEC_PER_SEC as i64 -
                                              other.t.tv_nsec as i64)
                }
            }
        }

        impl Sub<Duration> for SteadyTime {
            type Output = SteadyTime;
            fn sub(self, other: Duration) -> SteadyTime {
                self + -other
            }
        }

        impl Add<Duration> for SteadyTime {
            type Output = SteadyTime;
            fn add(mut self, other: Duration) -> SteadyTime {
                let seconds = other.num_seconds();
                let nanoseconds = other - Duration::seconds(seconds);
                let nanoseconds = nanoseconds.num_nanoseconds().unwrap();
                self.t.tv_sec += seconds as libc::time_t;
                self.t.tv_nsec += nanoseconds as libc::c_long;
                if self.t.tv_nsec >= ::NSEC_PER_SEC as libc::c_long {
                    self.t.tv_nsec -= ::NSEC_PER_SEC as libc::c_long;
                    self.t.tv_sec += 1;
                } else if self.t.tv_nsec < 0 {
                    self.t.tv_sec -= 1;
                    self.t.tv_nsec += ::NSEC_PER_SEC as libc::c_long;
                }
                self
            }
        }

        impl PartialOrd for SteadyTime {
            fn partial_cmp(&self, other: &SteadyTime) -> Option<Ordering> {
                Some(self.cmp(other))
            }
        }

        impl Ord for SteadyTime {
            fn cmp(&self, other: &SteadyTime) -> Ordering {
                match self.t.tv_sec.cmp(&other.t.tv_sec) {
                    Ordering::Equal => self.t.tv_nsec.cmp(&other.t.tv_nsec),
                    ord => ord
                }
            }
        }

        impl PartialEq for SteadyTime {
            fn eq(&self, other: &SteadyTime) -> bool {
                self.t.tv_sec == other.t.tv_sec &&
                    self.t.tv_nsec == other.t.tv_nsec
            }
        }

        impl Eq for SteadyTime {}

    }
}

