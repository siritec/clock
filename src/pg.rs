use std::io::Write;

use ::{Duration, Timespec, Tm, EpochM, empty_tm, USEC_PER_SEC};
use ::sys;

use diesel::deserialize::{self, FromSql};
use diesel::pg::Pg;
use diesel::serialize::{self, Output, ToSql};
use diesel::sql_types::{self, Timestamp, Timestamptz, Date, BigInt};
use diesel::data_types::{PgDate, PgTime, PgTimestamp};

const PG_EPOCH: EpochM = 946_684_800_000_000;
const PG_EPOCH_DATE: Tm = Tm {
    tm_sec: 0,
    tm_min: 0,
    tm_hour: 0,
    tm_mday: 1,
    tm_mon: 0,
    tm_year: 100,
    tm_wday: 6,
    tm_yday: 0,
    tm_isdst: 0,
    tm_utcoff: 0,
    tm_nsec: 0,
};

/// Returns the specified time in the local timezone
pub fn at_epoch_m(usec: i64) -> Tm {
    let mut tm = empty_tm();
    let s = usec / USEC_PER_SEC as i64;
    let u = (usec % USEC_PER_SEC as i64) as i32;
    sys::time_to_local_tm(s, &mut tm);
    tm.tm_nsec = u * 1_000;
    tm
}

impl ToSql<Timestamp, Pg> for Timespec {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Pg>) -> serialize::Result {
        let pg_epoch = Timespec::new(PG_EPOCH as i64, 0);
        let duration = *self - pg_epoch;
        let t = try!(duration.num_microseconds().ok_or("Overflow error"));
        ToSql::<BigInt, Pg>::to_sql(&t, out)
    }
}

impl FromSql<sql_types::Timestamp, Pg> for Timespec {
    fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
        let t = try!(<i64 as FromSql<BigInt, Pg>>::from_sql(bytes));
        let pg_epoch = Timespec::new(PG_EPOCH as i64, 0);
        let duration = Duration::microseconds(t);
        let out = pg_epoch + duration;
        Ok(out)
    }
}

impl ToSql<Timestamp, Pg> for Tm {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Pg>) -> serialize::Result {
        let t = self.to_timespec();
        ToSql::<Timestamp, Pg>::to_sql(&t, out)
    }
}

impl FromSql<Timestamp, Pg> for Tm {
    fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {

        let t = <i64 as FromSql<BigInt, Pg>>::from_sql(bytes)?;

        let out = at_epoch_m(t + PG_EPOCH as i64);
        Ok(out)
    }
}

impl ToSql<Date, Pg> for Tm {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Pg>) -> serialize::Result {
        let t = self.to_timespec();
        let days_since_epoch = (*self - PG_EPOCH_DATE).num_days();
        ToSql::<Date, Pg>::to_sql(&PgDate(days_since_epoch as i32), out)
    }
}

impl FromSql<Date, Pg> for Tm {
    fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {

        let PgDate(offset) = try!(FromSql::<Date, Pg>::from_sql(bytes));
        let out = PG_EPOCH_DATE + Duration::days(offset as i64);

        Ok(out)
    }
}
