use super::{TimerError, EpochN, precise_time_ns};

pub struct Timer {
    start: EpochN,
    recs: Vec<EpochN>,
}

impl Timer {
    pub fn new() -> Timer {
        Timer {
            start: 0,
            recs: Vec::new(),
        }
    }

    pub fn start(&mut self) {
        self.recs.clear();
        self.start = precise_time_ns()
    }

    pub fn tap(&mut self) -> Result<EpochN, TimerError>  {
        if self.start == 0 {
            Err(TimerError::TimerNotInit)
        } else {
            let d = ::precise_time_ns() - self.start;
            self.recs.push(d);
            Ok(d)
        }
    }
}
