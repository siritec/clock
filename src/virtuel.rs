const NANO_PER_SEC : u64 = 1_000_000_000;
const SEC_PER_DAY : u64 = 86400;
const MAX_ITER_SEC : usize = SEC_PER_DAY as usize;

pub trait ClockLike<T>: Iterator<Item=T> {
    fn sync(&mut self, as_of: &T);
    fn now(&self) -> Option<T>;
    fn elapse_unit(&mut self) -> Option<T> {
        self.next()
    }
}


pub struct VirtualClock<T> {
    zero_hour: T,
    chronon: u64,
    elapsed: usize,
}

impl VirtualClock<u64> {
    pub fn new(zero_hour_: u64, chronon_: u64) -> VirtualClock<u64> {
        VirtualClock {
            zero_hour: zero_hour_,
            chronon: chronon_,
            elapsed: 0
        }
    }
}

impl Iterator for VirtualClock<u64> {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        if (self.chronon * self.elapsed as u64) < NANO_PER_SEC * MAX_ITER_SEC as u64 {
            self.elapsed += 1;
            self.zero_hour.checked_add(self.chronon * self.elapsed as u64)
        } else {
            None
        }
    }
}

impl ClockLike<u64> for VirtualClock<u64> {
    fn sync(&mut self, as_of: &u64) {
        self.zero_hour = *as_of;
        self.elapsed = 0;
    }

    fn now(&self) -> Option<u64> {
        if let Some(d) = self.chronon.checked_mul(self.elapsed as u64) {
            d.checked_add(self.zero_hour)
        } else {
            None
        }
    }
}
